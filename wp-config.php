<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'kingster' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Bi}ZGZWti1?2V4$.J7ErOXtc98m%qoFKqW;Qrj,97XNSr([]azc:eKqJfD/T=43}' );
define( 'SECURE_AUTH_KEY',  'l35,45J6b|0CjCZ%JENOy!1^A{XDa2 evzx8wsry%0S9JIif=X]%_O+J=>C|I7B7' );
define( 'LOGGED_IN_KEY',    '#VuS8>Cx{&0Ip15~GW7!7s$tNL1PC.g=#Q/co+t ^zTR=V;.P J5sFfJd,i,;|q2' );
define( 'NONCE_KEY',        '3Mzq[}a[H~C jf;en)J^%(@>Y_sJl2@:TT{lt13q)3c&HeL%ofSptL-Q?uHn~]] ' );
define( 'AUTH_SALT',        '@H@[E1F35%2F/`Hx#B.3-@H-54K0A,=ou$^F?9PAwhFb|?(:jO9</}B|/=iT/a&P' );
define( 'SECURE_AUTH_SALT', '(B(dh]zG)pZ:byhB !czWSq)o*<jau~=e9W=;3e)K#[$ zoi+ >2B^_I04d!Dm3A' );
define( 'LOGGED_IN_SALT',   'l_JDY(i]0eQKv}?sA9-6!5*}r>I5PT3}0-+hm(<.?h/o9Tir*PG8,x~*DbNT)!]u' );
define( 'NONCE_SALT',       'IU|]*W#YNt1;QF XV6}eHgOsN&}TypBr{R KyRtp=7}1/>RH3)/4xbd-ob3rjP(U' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
