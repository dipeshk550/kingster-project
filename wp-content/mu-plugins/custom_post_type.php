<?php
add_action('init', 'kingster_slider_post_type', 0 );


function kingster_slider_post_type() {
    // Labels for the Post Type
    $labels = array(
        'name'                => _x( 'Sliders', 'Post Type General Name', 'corporex' ),
        'singular_name'       => _x( 'Slider', 'Post Type Singular Name', 'corporex' ),
        'menu_name'           => __( 'Sliders', 'corporex' ),
        'parent_item_colon'   => __( 'Parent Slider', 'corporex' ),
        'all_items'           => __( 'All Sliders', 'corporex' ),
        'view_item'           => __( 'View Slider', 'corporex' ),
        'add_new_item'        => __( 'Add New Slider', 'corporex' ),
        'add_new'             => __( 'Add New Slider', 'corporex' ),
        'edit_item'           => __( 'Edit Slider', 'corporex' ),
        'update_item'         => __( 'Update Slider', 'corporex' ),
        'search_items'        => __( 'Search Slider', 'corporex' ),
        'not_found'           => __( 'No sliders found', 'corporex' ),
        'not_found_in_trash'  => __( 'Not found in trash', 'corporex' ),
    );
    // Another Customizations
    $args = array(
        'label'   => __('Sliders','corporex' ),
        'description' => __('Sliders for Mount', 'corporex'),
        'labels'  => $labels,
        'supports' => array('title', 'thumbnail','editor'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menus' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 15,
        'menu_icon' => 'dashicons-camera-alt',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'capability_type' => 'page',
    );
    // register the post Type
    register_post_type( 'sliders', $args);
}