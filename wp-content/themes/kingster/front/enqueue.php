<?php
   function kingster_styles(){
        $uri = get_theme_file_uri();
        //CSS
        wp_register_style('style', $uri . '/assets/plugins/goodlayers-core/plugins/combine/style.css');
        wp_register_style('page_builer', $uri . '/assets/plugins/goodlayers-core/include/css/page-builder.css');
        wp_register_style('settings', $uri . '/assets/plugins/revslider/public/assets/css/settings.css');
        wp_register_style('style_core', $uri . '/assets/css/style-core.css');
        wp_register_style('custom', $uri . '/assets/css/kingster-style-custom.css');
        wp_register_style('googleapi', 'https://fonts.googleapis.com/css?family=Playfair+Display:700,400');
        wp_register_style('fonts', 'https://fonts.googleapis.com/css?family=Poppins:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7CABeeZee:regular,italic&subset=latin,latin-ext,devanagari&ver=5.0.3');
        wp_enqueue_style('style');
        wp_enqueue_style('page_builer');
        wp_enqueue_style('settings');
        wp_enqueue_style('style_core');
        wp_enqueue_style('custom');
        wp_enqueue_style('googleapi');
        wp_enqueue_style('fonts');
        //JS
       wp_register_script('tools', $uri . '/assets/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js', [], false, true);
       wp_register_script('revolution', $uri . '/assets/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js', [], false, true);
       wp_register_script('slideanims', $uri . '/assets/plugins/revslider/public/assets/js/extensions/revolution.extension.slideanims.min.js', [], false, true);
       wp_register_script('layer', $uri . '/assets/plugins/revslider/public/assets/js/extensions/revolution.extension.layeranimation.min.js', [], false, true);
       wp_register_script('kenburn', $uri . '/assets/plugins/revslider/public/assets/js/extensions/revolution.extension.kenburn.min.js', [], false, true);
       wp_register_script('nav', $uri . '/assets/plugins/revslider/public/assets/js/extensions/revolution.extension.navigation.min.js', [], false, true);
       wp_register_script('parralax', $uri . '/assets/plugins/revslider/public/assets/js/extensions/revolution.extension.parallax.min.js', [], false, true);
       wp_register_script('actions', $uri . '/assets/plugins/revslider/public/assets/js/extensions/revolution.extension.actions.min.js', [], false, true);
       wp_register_script('video', $uri . '/assets/plugins/revslider/public/assets/js/extensions/revolution.extension.video.min.js', [], false, true);
       wp_register_script('plugin', $uri . '/assets/js/plugins.min.js', [], false, true);
       wp_register_script('script', $uri . '/assets/plugins/goodlayers-core/plugins/combine/script.js', [], false, true);
       wp_register_script('pagebuilder', $uri . '/assets/plugins/goodlayers-core/include/js/page-builder.js', [], false, true);
       wp_register_script('effect', $uri . '/assets/js/jquery/ui/effect.min.js', [], false, true);
       wp_enqueue_script('jquery');
       wp_enqueue_script('tools');
       wp_enqueue_script('revolution');
       wp_enqueue_script('slideanims');
       wp_enqueue_script('layer');
       wp_enqueue_script('kenburn');
       wp_enqueue_script('nav');
       wp_enqueue_script('parralax');
       wp_enqueue_script('actions');
       wp_enqueue_script('video');
       wp_enqueue_script('plugin');
       wp_enqueue_script('script');
       wp_enqueue_script('pagebuilder');
       wp_enqueue_script('effect');
   }




