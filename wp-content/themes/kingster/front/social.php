<?php
   function kingster_social_customize_section($wp_customize){
       $wp_customize->add_setting('kingster_facebook_handle', array(
           'default' => ''
       ));
       $wp_customize->add_setting('kingster_twitter_handle', array(
           'default' => ''
       ));
       $wp_customize->add_setting('kingster_linkedin_handle', array(
           'default' => ''
       ));
       $wp_customize->add_setting('kingster_instagram_handle', array(
           'default' => ''
       ));
       $wp_customize->add_section('kingster_social_section', [
           'title' => __('Social Media Settings', 'kingster'),
           'priority' => 30,
           'panel' => 'basic'

       ]);
       $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'kingster_facebook_text_handle', array(
           'label' => __('Facebook', 'kingster'),
           'section' => 'kingster_social_section',
           'settings' => 'kingster_facebook_handle',
           'description' => __('Please Enter Your Facebook URL', 'kingster'),
           'type' => 'text'
       )));
       $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'kingster_twitter_text_handle', array(
           'label' => __('Twitter', 'kingster'),
           'section' => 'kingster_social_section',
           'settings' => 'kingster_twitter_handle',
           'description' => __('Please Enter Your twitter URL', 'kingster'),
           'type' => 'text'
       )));
       $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'kingster_linkedin_text_handle', array(
           'label' => __('Linkedin', 'kingster'),
           'section' => 'kingster_social_section',
           'settings' => 'kingster_linkedin_handle',
           'description' => __('Please Enter Your Linkedin URL', 'kingster'),
           'type' => 'text'
       )));
       $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'kingster_instagram_text_handle', array(
           'label' => __('Instagram', 'kingster'),
           'section' => 'kingster_social_section',
           'settings' => 'kingster_instagram_handle',
           'description' => __('Please Enter Your Instagram URL', 'kingster'),
           'type' => 'text'
       )));
   }